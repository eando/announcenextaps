<form action="nextmeeting_confirm.php" method="post">
<?php
// 設定ファイル読み込み
$path='./conf/setting.ini';
$config=parse_ini_file($path, false);

// ファイルパス取得
$file_placelist=$config['FILE_PLACELIST'];
?>

<dl>
	<dt>次回開催日時</dt>
	<dd>
		<input type="date" name="nextDate"></input>
		<input type="text" name="startTime" value="10:30" size="5" maxlength="3" />〜
		<input type="text" name="endTime" value="12:30" size="5" maxlength="3" />
	</dd>

	<dt>開催場所</dt>
	<dd>
		<select name="place" id="place">
		<?php
		$row = 1;
		// ファイルが存在しているかチェックする
		if (($handle = fopen($file_placelist, "r")) !== FALSE) {
			// 1行ずつfgetcsv()関数を使って読み込む
			while (($data = fgetcsv($handle))) {
				print('<option value="' . $row . '">' . $data[0] . '</option>');
				$row++;
			}
			fclose($handle);
		}
		?>
		</select>
		<a href= "./addplace.php" >場所の追加・削除</a>
	</dd>

	<dt>会場料金</dt>
	<dd>
		<input type="text" name="fee" value="2080" size="5" maxlength="5" />円
	</dd>

</dl>

<input type="submit" value="送信する" />
</form>
