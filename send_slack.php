<?php
// 設定ファイル読み込み
$path='./conf/setting.ini';
$config=parse_ini_file($path, false);

$url_webhook=$config['URL_WEBHOOK'];
$file_announce=$config['FILE_ANNOUNCE'];
$file_reminder=$config['FILE_REMINDER'];

$text_announce=file_get_contents($file_announce);
$text_reminder=file_get_contents($file_reminder);

// Slack通知関数の定義
function send_to_slack($message, $url_webhook) {
  $options = array(
    'http' => array(
      'method' => 'POST',
      'header' => 'Content-Type: application/json',
      'content' => json_encode($message),
    )
  );
  $response = file_get_contents($url_webhook, false, stream_context_create($options));
  return $response === 'ok';
}

$message = array(
  'username' => 'Announce',
  'text' => $text_announce,
);

// Slack通知
send_to_slack($message, $url_webhook);

// Slackリマインダ文案を画面表示
print('=== 以下はリマインダ用の文案です。コピーしてSlackへ投稿してください。 ===');
print('<br>');
print($text_reminder);
?>
