<?php
// 設定ファイル読み込み
$path='./conf/setting.ini';
$config=parse_ini_file($path, false);

// ファイルパス取得
$file_placelist=$config['FILE_PLACELIST'];

// htmlspecialcharsのショートカット
function h($value) {
    return htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
}

// 削除する行番号を取得
$id=h($_REQUEST['id']);

// 削除対象の行を削除
$file=file($file_placelist);
unset($file[$id-1]);
file_put_contents($file_placelist, $file);
?>
<p>削除しました</p>
<p><a href="nextmeeting.php">トップページに戻る</a></p>
