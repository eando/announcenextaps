<form action="addplace_confirm.php" method="post">
<?php
// 設定ファイル読み込み
$path='./conf/setting.ini';
$config=parse_ini_file($path, false);

// ファイルパス取得
$file_placelist=$config['FILE_PLACELIST'];
?>

<p> 開催場所の追加 </p>
カンマ(,)は入力不可
<dl>
	<dt>場所名</dt>
	<dd>
		<input pattern="[^,]*" type="text" name="name" size="50" maxlength="80" required/>※入力必須
	</dd>

	<dt>住所</dt>
	<dd>
		<input pattern="[^,]*" type="text" name="address" size="100" maxlength="500" />
	</dd>
	<dt>URL</dt>
	<dd>
		<input pattern="[^,]*" type="url" name="url" size="100" maxlength="2000" />
	</dd>
</dl>
<input type="submit" value="登録する" />
</form>

<br><br><br>

<p>開催場所リスト</p>
<table width="100%" border="1" style="border-collapse: collapse">
	<tr>
		<th scope="col">場所名</th>
		<th scope="col">住所</th>
		<th scope="col">URL</th>
		<th scope="col">削除</th>
	</tr>

	<?php
	// 確認ダイアログに表示する文言
	$message_confirm='\'削除してよろしいですか？\'';
	// 開催場所リストの読み込み
	$row = 1;
	// ファイルが存在しているかチェックする
	if (($handle = fopen($file_placelist, "r")) !== FALSE) {
		// 1行ずつfgetcsv()関数を使って読み込む
		while (($data = fgetcsv($handle))) {
			print('<tr>');
			print('<td>'. $data[0] . '</td>');
			print('<td>'. $data[1] . '</td>');
			print('<td>'. $data[2] . '</td>');
			print('<td>'. '<a href="delete.php?id=' . $row . '" onclick="return confirm(' . $message_confirm . ');">削除</a>' . '</td>');
			print('</tr>');
			$row++;
		}
		fclose($handle);
	}
	?>
</table>
