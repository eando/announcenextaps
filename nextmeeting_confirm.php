<form action="send_slack.php" method="post">

<?php
// 設定ファイル読み込み
$path='./conf/setting.ini';
$config=parse_ini_file($path, false);

// ファイルパス取得
$file_placelist=$config['FILE_PLACELIST'];
$file_announce=$config['FILE_ANNOUNCE'];
$file_reminder=$config['FILE_REMINDER'];

// htmlspecialcharsのショートカット
function h($value) {
	return htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
}

// フォームの入力値を設定
$nextDate=h($_POST['nextDate']);
$startTime=h($_POST['startTime']);
$endTime=h($_POST['endTime']);
$fee=h($_POST['fee']);
$place=h($_POST['place']);

// 開催場所の読み込み
$row = 1;
// ファイルが存在しているかチェックする
if (($handle = fopen($file_placelist, "r")) !== FALSE) {
	// 1行ずつfgetcsv()関数を使って読み込む
	while (($data = fgetcsv($handle))) {
		// フォームで選択した場所($place)と一致したら情報を格納してbreak
		if ($row == $place) {
			$placeName = $data[0];
			$placeAddress = $data[1];
			$placeURL = $data[2];
			break;
		}
		$row++;
	}
	fclose($handle);
}

// 指定日の曜日を取得する
$youbi_num = date('w', strtotime($nextDate));
 
//配列を使用し、要素順に(日:0〜土:6)を設定する
$week = [
  '日', //0
  '月', //1
  '火', //2
  '水', //3
  '木', //4
  '金', //5
  '土', //6
];
// 曜日の日本語名を取得（日,月,火,水,木,金,土)
$youbi_jp = $week[$youbi_num];

// リマインダ日：開催日の前日
$date_remind = date('Y-m-d', strtotime($nextDate . " -1 day"));

// Slackに通知する文案を作成
// リマインダ文案には$message0,$message6を追加
$message0='/remind #general <br>';
$message1='<!channel>【案内】次回の「主催者の課題を情報技術で解決する勉強会」は以下のとおり開催します。' ;
$message2='開催日時：' . date('Y/m/d', strtotime($nextDate)) . '(' . $youbi_jp . ')' . $startTime . '-' . $endTime ;
$message3='会場料金：' . $fee . '円（会費は割り勘。端数が生じる場合あり）' ;
$message4_1='場所：' . $placeName ;
$message4_2='住所：' . $placeAddress ;
$message4_3='URL：' . $placeURL ;
$message5='当日の連絡は本スレッドにお願いいたします。';
$message6='<br>' . $date_remind;

// HTML表示用文案
$messageAnnounceHTML = $message1 . '<br>' . $message2 . '<br>' . $message3 . '<br>' . $message4_1 . '<br>' . $message4_2 . '<br>' . $message4_3 . '<br>' . $message5 . $message6;
//$messageAnnounceHTML = $message1 . '<br>' . $message2 . '<br>' . $message3 . '<br>' . $message4_1 . '<br>' . $message4_2 . '<br>' . $message4_3 . '<br>' . $message5;

// Slack通知用文案
$messageAnnounceSlack = $message1 . PHP_EOL . $message2 . PHP_EOL . $message3 . PHP_EOL . $message4_1 . PHP_EOL . $message4_2 . PHP_EOL . $message4_3 . PHP_EOL . $message5 ;

// Slackリマインダ用文案
$messageReminderSlack = $message0 . $messageAnnounceSlack . $message6 ;

// Slack用の文案をファイル書き込み
file_put_contents($file_announce , $messageAnnounceSlack);
file_put_contents($file_reminder , $messageReminderSlack);

print('===== 以下の内容を確認し、問題なければ「Slack投稿」ボタンを押下してください。 =====');
print('<br>');
print($messageAnnounceHTML);
print('<br>');

// Slack投稿ボタン
print('<button type="submit" name="sendSlack" value="">Slack投稿</button>');
?>

</form>
