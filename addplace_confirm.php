<?php
// 設定ファイル読み込み
$path='./conf/setting.ini';
$config=parse_ini_file($path, false);

// ファイルパス取得
$file_placelist=$config['FILE_PLACELIST'];

// htmlspecialcharsのショートカット
function h($value) {
    return htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
}

$doc = file_get_contents($file_placelist);
$doc .= h($_POST["name"]) . ',' . h($_POST["address"]) . ',' . h($_POST["url"]) . PHP_EOL;
file_put_contents($file_placelist, $doc);
?>
<p>追加しました</p>
<p><a href="nextmeeting.php">トップページに戻る</a></p>
<br>

<p>開催場所リスト</p>
<table width="100%" border="1" style="border-collapse: collapse">
	<tr>
		<th scope="col">場所名</th>
		<th scope="col">住所</th>
		<th scope="col">URL</th>
	</tr>

	<?php
	// 開催場所リストの読み込み
	$row = 1;
	// ファイルが存在しているかチェックする
	if (($handle = fopen($file_placelist, "r")) !== FALSE) {
		// 1行ずつfgetcsv()関数を使って読み込む
		while (($data = fgetcsv($handle))) {
			print('<tr>');
			print('<td>'. $data[0] . '</td>');
			print('<td>'. $data[1] . '</td>');
			print('<td>'. $data[2] . '</td>');
			print('</tr>');
			$row++;
		}
		fclose($handle);
	}
	?>
</table>
